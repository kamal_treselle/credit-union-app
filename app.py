import sys
from flask_cors import CORS
from flask import Flask

app = Flask(__name__)
CORS(app)


@app.route('/', methods=['GET', 'POST'])
def login():
    return "Hello World"


if __name__ == '__main__':
    # Start app
    app.run(host='0.0.0.0', port=5001, threaded=True, debug=True)
